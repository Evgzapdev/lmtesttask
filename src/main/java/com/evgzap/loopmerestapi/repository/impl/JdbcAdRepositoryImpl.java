package com.evgzap.loopmerestapi.repository.impl;

import com.evgzap.loopmerestapi.model.entity.Ad;
import com.evgzap.loopmerestapi.model.input.AdInputDto;
import com.evgzap.loopmerestapi.repository.AdRepository;
import com.evgzap.loopmerestapi.repository.extractor.AdByIdResultSetExtractor;
import com.evgzap.loopmerestapi.value.AdPlatform;
import lombok.AllArgsConstructor;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.Optional;

@Repository
@AllArgsConstructor
public class JdbcAdRepositoryImpl implements AdRepository {

    private JdbcTemplate jdbcTemplate;

    @Override
    public int save(AdInputDto ad) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(con -> {
            PreparedStatement statement = con.prepareStatement(
                    "INSERT INTO ADS (name, status, assert_url) values (?,?,?)",
                    Statement.RETURN_GENERATED_KEYS);
            statement.setString(1, ad.getName());
            statement.setInt(2, ad.getStatus().getIdx());
            statement.setString(3, ad.getAssertUrl());
            return statement;
        }, keyHolder);

        int adId = keyHolder.getKey().intValue();

        for (AdPlatform platform : ad.getPlatforms()) {
            jdbcTemplate.update(
                    "INSERT INTO ADS_TO_PLATFORMS (ad_id, platform) values (?,?)",
                    adId,
                    platform.getIdx());
        }

        jdbcTemplate.update(
                "INSERT INTO CAMPAIGNS_TO_ADS (campaign_id, ad_id) values (?,?)",
                ad.getCampaignId(),
                adId);
        return adId;
    }

    @Override
    public int update(int id, AdInputDto ad) {
        jdbcTemplate.update(con -> {
            PreparedStatement statement = con.prepareStatement(
                    "UPDATE ADS  SET  name = ? , status = ? , assert_url = ? where id = ?",
                    Statement.RETURN_GENERATED_KEYS);
            statement.setString(1, ad.getName());
            statement.setInt(2, ad.getStatus().getIdx());
            statement.setString(3, ad.getAssertUrl());
            statement.setInt(4, id);
            return statement;
        });

        jdbcTemplate.update("DELETE FROM ADS_TO_PLATFORMS WHERE ad_id = ?", id);
        for (AdPlatform platform : ad.getPlatforms()) {
            jdbcTemplate.update(
                    "INSERT INTO ADS_TO_PLATFORMS (ad_id, platform) values (?,?)",
                    id,
                    platform.getIdx());
        }
        return id;
    }

    @Override
    public int deleteById(int id) {
        return jdbcTemplate.update("DELETE FROM ADS WHERE id = ?", id);
    }

    @Override
    public Optional<Ad> findById(int id) {
        return jdbcTemplate.query("SELECT ADS.id as id, ADS.name as name, ADS.status as status, ADS.assert_url as assert_url, ATP.platform as platform " +
                "FROM ADS LEFT JOIN ADS_TO_PLATFORMS ATP on ADS.id = ATP.ad_id WHERE ADS.id = ? ",new Object[]{id}, new AdByIdResultSetExtractor());
    }
}
