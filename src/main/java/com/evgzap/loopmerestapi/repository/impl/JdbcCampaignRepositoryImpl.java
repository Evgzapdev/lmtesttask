package com.evgzap.loopmerestapi.repository.impl;

import com.evgzap.loopmerestapi.model.entity.Campaign;
import com.evgzap.loopmerestapi.model.entity.CampaignShort;
import com.evgzap.loopmerestapi.model.input.AdInputDto;
import com.evgzap.loopmerestapi.model.input.CampaignInputDto;
import com.evgzap.loopmerestapi.model.pageable.Filter;
import com.evgzap.loopmerestapi.model.pageable.Pageable;
import com.evgzap.loopmerestapi.model.pageable.Sort;
import com.evgzap.loopmerestapi.repository.AdRepository;
import com.evgzap.loopmerestapi.repository.CampaignRepository;
import com.evgzap.loopmerestapi.repository.extractor.CampaignsByIdResultSetExtractor;
import com.evgzap.loopmerestapi.value.AdPlatform;
import com.evgzap.loopmerestapi.value.Status;
import lombok.AllArgsConstructor;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;

@Repository
@AllArgsConstructor
public class JdbcCampaignRepositoryImpl implements CampaignRepository {

    private AdRepository adRepository;

    private JdbcTemplate jdbcTemplate;

    @Override
    public int save(CampaignInputDto campaign) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(con -> {
            PreparedStatement statement = con.prepareStatement(
                    "INSERT INTO CAMPAIGNS (name, status, start_date, end_date) values (?,?,?,?)",
                    Statement.RETURN_GENERATED_KEYS);
            statement.setString(1, campaign.getName());
            statement.setInt(2, campaign.getStatus().getIdx());
            statement.setTimestamp(3, Timestamp.valueOf(campaign.getStart_date()));
            statement.setTimestamp(4, Timestamp.valueOf(campaign.getStart_date()));
            return statement;
        }, keyHolder);

        int campaignId = keyHolder.getKey().intValue();

        for (AdInputDto ad : campaign.getAds()) {
            KeyHolder adKeyHolder = new GeneratedKeyHolder();
            jdbcTemplate.update(con -> {
                PreparedStatement statement = con.prepareStatement(
                        "INSERT INTO ADS (name, status, assert_url) values (?,?,?)",
                        Statement.RETURN_GENERATED_KEYS);
                statement.setString(1, ad.getName());
                statement.setInt(2, ad.getStatus().getIdx());
                statement.setString(3, ad.getAssertUrl());
                return statement;
            }, adKeyHolder);

            int adId = adKeyHolder.getKey().intValue();

            for (AdPlatform platform : ad.getPlatforms()) {
                jdbcTemplate.update(
                        "INSERT INTO ADS_TO_PLATFORMS (ad_id, platform) values (?,?)",
                        adId,
                        platform.getIdx());
            }

            jdbcTemplate.update(
                    "INSERT INTO CAMPAIGNS_TO_ADS (campaign_id, ad_id) values (?,?)",
                    campaignId,
                    adId);
        }
        return campaignId;
    }

    @Override
    public int update(int id, CampaignInputDto campaign) {
        jdbcTemplate.update(con -> {
            PreparedStatement statement = con.prepareStatement(
                    "UPDATE CAMPAIGNS set name = ? , status = ? , start_date = ?, end_date = ? where id = ?");
            statement.setString(1, campaign.getName());
            statement.setInt(2, campaign.getStatus().getIdx());
            statement.setTimestamp(3, Timestamp.valueOf(campaign.getStart_date()));
            statement.setTimestamp(4, Timestamp.valueOf(campaign.getStart_date()));
            statement.setInt(5, id);
            return statement;
        });

        return id;
    }

    @Override
    public int deleteById(int id) {
        return jdbcTemplate.update("DELETE FROM CAMPAIGNS WHERE id = ?", id);
    }

    @Override
    public List<CampaignShort> findAllShortCampaign(Pageable pageable, Filter filter) {
        String qu = "SELECT CAMPAIGNS.id, CAMPAIGNS.name, CAMPAIGNS.status, COUNT(CTA.campaign_id) ad_count FROM CAMPAIGNS LEFT JOIN CAMPAIGNS_TO_ADS CTA on CAMPAIGNS.id = CTA.campaign_id";
        if (filter != null && (filter.getFilterName() != null || filter.getFilterStatus() != null)) {
            qu += " WHERE ";
            if (filter.getFilterName() != null) {
                qu += "name LIKE '%" + filter.getFilterName() +"%' ";
            }
            if (filter.getFilterName() != null && filter.getFilterStatus() != null){
                qu += " AND ";
            }
            if (filter.getFilterStatus() != null) {
                qu += "status = '" + filter.getFilterStatus() + "' ";
            }
        }
        qu += " GROUP BY 1,2,3 ";
        if (pageable != null) {
            if (pageable.getSorts() != null && !pageable.getSorts().isEmpty()) {
                qu += " ORDER BY ";
                List<Sort> sorts = pageable.getSorts();
                for (int i = 0; i < sorts.size(); i++) {
                    Sort sort = sorts.get(i);
                    qu += sort.getBy() + " " + sort.getDirection();
                    if (i < sorts.size() - 1) {
                        qu += ",";
                    }
                }
            }
            int pageSize = pageable.getPageSize() > 0 ? pageable.getPageSize() : 10;
            qu += " LIMIT " + pageable.getPageNumber() * pageSize + "," + pageSize + " ";
        }
        return jdbcTemplate.query(qu,
                (rs, i) -> new CampaignShort(
                        rs.getInt("id"),
                        rs.getString("name"),
                        Status.fromIdx(rs.getInt("status")),
                        rs.getInt("ad_count")));
    }

    @Override
    public Optional<Campaign> findById(int id) {
        CampaignsByIdResultSetExtractor rse = new CampaignsByIdResultSetExtractor();
        return jdbcTemplate.query("SELECT CAMPAIGNS.id as id, " +
                "       CAMPAIGNS.name   as name, " +
                "       CAMPAIGNS.status as status, " +
                "       CAMPAIGNS.start_date as start_date, " +
                "       CAMPAIGNS.end_date as end_date, " +
                "       ADS.id as ad_id, " +
                "       ADS.name as ad_name, " +
                "       ADS.status as ad_status, " +
                "       ADS.assert_url as ad_assert_url, " +
                "       ADS_TO_PLATFORMS.platform as ad_platform " +
                "FROM CAMPAIGNS " +
                "LEFT JOIN CAMPAIGNS_TO_ADS CTA on CAMPAIGNS.id = CTA.campaign_id " +
                "LEFT JOIN ADS on CTA.ad_id = ADS.id " +
                "LEFT JOIN ADS_TO_PLATFORMS on ADS.id = ADS_TO_PLATFORMS.ad_id " +
                "WHERE CAMPAIGNS.id = ?", new Object[]{id}, rse);
    }
}
