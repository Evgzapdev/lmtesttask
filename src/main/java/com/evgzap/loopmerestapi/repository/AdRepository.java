package com.evgzap.loopmerestapi.repository;

import com.evgzap.loopmerestapi.model.entity.Ad;
import com.evgzap.loopmerestapi.model.input.AdInputDto;

import java.util.Optional;

public interface AdRepository {

    public int save(AdInputDto ad);
    public int update(int id, AdInputDto ad);
    public int deleteById(int id);
    public Optional<Ad> findById(int id);
}
