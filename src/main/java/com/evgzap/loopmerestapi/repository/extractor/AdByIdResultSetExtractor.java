package com.evgzap.loopmerestapi.repository.extractor;

import com.evgzap.loopmerestapi.model.entity.Ad;
import com.evgzap.loopmerestapi.value.AdPlatform;
import com.evgzap.loopmerestapi.value.Status;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class AdByIdResultSetExtractor implements ResultSetExtractor<Optional<Ad>> {
    @Override
    public Optional<Ad> extractData(ResultSet rs) throws SQLException, DataAccessException {
        Map<Integer, Ad> adsById = new HashMap<>();
        while (rs.next()) {
            int id = rs.getInt("id");
            Ad ad = adsById.get(id);
            if (ad == null) {
                ad = new Ad(
                        id,
                        rs.getString("name"),
                        Status.fromIdx(rs.getInt("status")),
                        new ArrayList<>(),
                        rs.getString("assert_url")
                );
                adsById.put(id, ad);
            }
            ad.getPlatforms().add(AdPlatform.fromIdx(rs.getInt("platform")));
        }
        return adsById.values().stream().findFirst();
    }
}
