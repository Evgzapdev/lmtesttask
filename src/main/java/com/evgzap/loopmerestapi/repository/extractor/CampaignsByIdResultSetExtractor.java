package com.evgzap.loopmerestapi.repository.extractor;

import com.evgzap.loopmerestapi.model.entity.Ad;
import com.evgzap.loopmerestapi.model.entity.Campaign;
import com.evgzap.loopmerestapi.value.AdPlatform;
import com.evgzap.loopmerestapi.value.Status;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class CampaignsByIdResultSetExtractor implements ResultSetExtractor<Optional<Campaign>> {

    @Override
    public Optional<Campaign> extractData(ResultSet rs) throws SQLException, DataAccessException {
        Map<Integer, Campaign> campaignsById = new HashMap<>();
        while (rs.next()) {
            Integer id = rs.getInt("id");
            Campaign campaign = campaignsById.get(id);
            if (campaign == null) {
                campaign = new Campaign(
                        id,
                        rs.getString("name"),
                        Status.fromIdx(rs.getInt("status")),
                        rs.getTimestamp("start_date").toLocalDateTime(),
                        rs.getTimestamp("end_date").toLocalDateTime(),
                        new ArrayList<>());
                campaignsById.put(id, campaign);
            }
            int ad_id = rs.getInt("ad_id");
            Ad ad = campaign.getAds().stream().filter(a -> a.getId() == ad_id).findFirst().orElse(null);
            if (ad == null) {
                ad = new Ad(
                        ad_id,
                        rs.getString("ad_name"),
                        Status.fromIdx(rs.getInt("ad_status")),
                        new ArrayList<>(),
                        rs.getString("ad_assert_url"));
                campaign.getAds().add(ad);
            }
            ad.getPlatforms().add(AdPlatform.fromIdx(rs.getInt("ad_platform")));
        }
        return campaignsById.values().stream().findFirst();
    }
}
