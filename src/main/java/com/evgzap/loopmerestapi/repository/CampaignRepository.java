package com.evgzap.loopmerestapi.repository;

import com.evgzap.loopmerestapi.model.entity.Campaign;
import com.evgzap.loopmerestapi.model.entity.CampaignShort;
import com.evgzap.loopmerestapi.model.input.CampaignInputDto;
import com.evgzap.loopmerestapi.model.pageable.Filter;
import com.evgzap.loopmerestapi.model.pageable.Pageable;

import java.util.List;
import java.util.Optional;

public interface CampaignRepository {

    int save(CampaignInputDto ad);
    int update(int id, CampaignInputDto ad);
    int deleteById(int id);
    List<CampaignShort> findAllShortCampaign(Pageable pageable, Filter filter);
    Optional<Campaign> findById(int id);
}
