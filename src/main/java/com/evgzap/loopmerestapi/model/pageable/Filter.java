package com.evgzap.loopmerestapi.model.pageable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Filter {
    String filterName;
    String filterStatus;
}
