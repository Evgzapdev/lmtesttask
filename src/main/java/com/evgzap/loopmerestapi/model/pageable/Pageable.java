package com.evgzap.loopmerestapi.model.pageable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Pageable {
    int pageNumber;
    int pageSize;
    long offset;
    List<Sort> sorts;
}
