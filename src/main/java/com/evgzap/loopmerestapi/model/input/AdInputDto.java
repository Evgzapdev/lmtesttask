package com.evgzap.loopmerestapi.model.input;

import com.evgzap.loopmerestapi.value.AdPlatform;
import com.evgzap.loopmerestapi.value.Status;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AdInputDto {
    private int campaignId;
    private String name;
    private Status status;
    private List<AdPlatform> platforms;
    private String assertUrl;

}
