package com.evgzap.loopmerestapi.model.dto;

import com.evgzap.loopmerestapi.value.Status;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CampaignDto {

    private int id;
    private String name;
    private Status status;
    private LocalDateTime start_date;
    private LocalDateTime end_date;
    private List<AdDto> ads;

}
