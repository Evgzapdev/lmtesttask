package com.evgzap.loopmerestapi.model.dto;

import com.evgzap.loopmerestapi.value.Status;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CampaignShortDto {

    private int id;
    private String name;
    private Status status;
    private int adsCount;
}
