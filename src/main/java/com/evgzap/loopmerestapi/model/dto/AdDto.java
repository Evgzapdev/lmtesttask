package com.evgzap.loopmerestapi.model.dto;

import com.evgzap.loopmerestapi.value.AdPlatform;
import com.evgzap.loopmerestapi.value.Status;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class AdDto {

    private int id;
    private String name;
    private Status status;
    private List<AdPlatform> platforms;
    private String assertUrl;

}
