package com.evgzap.loopmerestapi.model.entity;

import com.evgzap.loopmerestapi.value.Status;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CampaignShort {

    private int id;
    private String name;
    private Status status;
    private int adsCount;
}
