package com.evgzap.loopmerestapi.model.entity;

import com.evgzap.loopmerestapi.value.AdPlatform;
import com.evgzap.loopmerestapi.value.Status;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class Ad {

    private int id;
    private String name;
    private Status status;
    private List<AdPlatform> platforms;
    private String assertUrl;

}
