package com.evgzap.loopmerestapi.model.entity;

import com.evgzap.loopmerestapi.value.Status;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
@AllArgsConstructor
public class Campaign {

    private int id;
    private String name;
    private Status status;
    private LocalDateTime start_date;
    private LocalDateTime end_date;
    private List<Ad> ads;

}
