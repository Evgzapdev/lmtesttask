package com.evgzap.loopmerestapi.value;

import lombok.Getter;

import java.util.Arrays;

@Getter
public enum Status {
    planned(0),
    active(1),
    paused(2),
    finished(3);

    private int idx;

    Status(int idx) {
        this.idx = idx;
    }

    public static Status fromIdx(int idx) {
        return Arrays.stream(values()).filter(status -> status.idx == idx)
                .findFirst().orElseThrow(() -> new IllegalArgumentException("Wrong CampaignStatus index"));
    }
}
