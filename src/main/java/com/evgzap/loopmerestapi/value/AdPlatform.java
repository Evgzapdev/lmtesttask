package com.evgzap.loopmerestapi.value;

import lombok.Getter;

import java.util.Arrays;

@Getter
public enum AdPlatform {
    web(0),
    android(1),
    ios(2);

    private int idx;

    AdPlatform(int idx) {
        this.idx = idx;
    }

    public static AdPlatform fromIdx(int idx) {
        return Arrays.stream(values()).filter(status -> status.idx == idx)
                .findFirst().orElseThrow(() -> new IllegalArgumentException("Wrong AdPlatform index"));
    }
}
