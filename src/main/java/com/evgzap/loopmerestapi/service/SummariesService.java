package com.evgzap.loopmerestapi.service;

import com.evgzap.loopmerestapi.model.dto.CampaignShortDto;
import com.evgzap.loopmerestapi.model.entity.CampaignShort;
import com.evgzap.loopmerestapi.model.pageable.Filter;
import com.evgzap.loopmerestapi.model.pageable.Pageable;
import com.evgzap.loopmerestapi.repository.CampaignRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class SummariesService {

    private CampaignRepository campaignRepository;

    public List<CampaignShortDto> getAllCampaignShort(Pageable pageable, Filter filter) {
        return campaignRepository.findAllShortCampaign(pageable, filter)
                .stream()
                .map(this::convertToDto)
                .collect(Collectors.toList());
    }

    private CampaignShortDto convertToDto(CampaignShort campaign) {
        return new CampaignShortDto(
                campaign.getId(),
                campaign.getName(),
                campaign.getStatus(),
                campaign.getAdsCount());
    }
}
