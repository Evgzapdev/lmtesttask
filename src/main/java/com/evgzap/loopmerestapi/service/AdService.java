package com.evgzap.loopmerestapi.service;

import com.evgzap.loopmerestapi.model.dto.AdDto;
import com.evgzap.loopmerestapi.model.entity.Ad;
import com.evgzap.loopmerestapi.model.input.AdInputDto;
import com.evgzap.loopmerestapi.repository.AdRepository;
import com.evgzap.loopmerestapi.repository.CampaignRepository;
import com.evgzap.loopmerestapi.value.exception.RecordNotFoundException;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class AdService {

    private AdRepository adRepository;
    private CampaignRepository campaignRepository;

    public AdDto create(AdInputDto adInput) throws RecordNotFoundException {
        campaignRepository.findById(adInput.getCampaignId())
                .orElseThrow(() -> new RecordNotFoundException(String.format("Campaign with id %s not found",
                        adInput.getCampaignId())));
        int id = adRepository.save(adInput);
        return getById(id);
    }

    public AdDto update(int id, AdInputDto adInput) throws RecordNotFoundException {
        int adId = adRepository.update(id, adInput);
        return getById(adId);
    }

    public void delete(int id) throws RecordNotFoundException {
        int i = adRepository.deleteById(id);
        if (i == 0) {
            throw new RecordNotFoundException(String.format("Can't delete ad with id %s; not found", id));
        }
    }

    public AdDto getById(int id) throws RecordNotFoundException {
        return convertToDto(adRepository.findById(id)
                .orElseThrow(() -> new RecordNotFoundException(String.format("Ad with id %s not found", id))));
    }


    AdDto convertToDto(Ad ad) {
        return new AdDto(
                ad.getId(),
                ad.getName(),
                ad.getStatus(),
                ad.getPlatforms(),
                ad.getAssertUrl()
        );
    }
}
