package com.evgzap.loopmerestapi.service;

import com.evgzap.loopmerestapi.model.dto.CampaignDto;
import com.evgzap.loopmerestapi.model.entity.Campaign;
import com.evgzap.loopmerestapi.model.input.CampaignInputDto;
import com.evgzap.loopmerestapi.repository.CampaignRepository;
import com.evgzap.loopmerestapi.value.exception.RecordNotFoundException;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class CampaignService {

    private CampaignRepository campaignRepository;

    private AdService adService;

    public CampaignDto create(CampaignInputDto inputDto) throws RecordNotFoundException {
        int id = campaignRepository.save(inputDto);
        return getCampaignById(id);
    }

    public CampaignDto update(int id, CampaignInputDto inputDto) throws RecordNotFoundException {
        int campaignId = campaignRepository.update(id, inputDto);
        return getCampaignById(campaignId);
    }

    public void delete(int id) throws RecordNotFoundException {
        int rows = campaignRepository.deleteById(id);
        if(rows == 0){
            throw new RecordNotFoundException(String.format("Can't delete campaign with id %s; not found", id));
        }
    }

    public CampaignDto getCampaignById(int id) throws RecordNotFoundException {
        return convertToDto(campaignRepository.findById(id)
                .orElseThrow(() -> new RecordNotFoundException(String.format("Campaign with id %s not found", id))));
    }

    private CampaignDto convertToDto(Campaign campaign) {
        return new CampaignDto(
                campaign.getId(),
                campaign.getName(),
                campaign.getStatus(),
                campaign.getStart_date(),
                campaign.getEnd_date(),
                campaign.getAds().stream().map(adService::convertToDto).collect(Collectors.toList()));
    }

}
