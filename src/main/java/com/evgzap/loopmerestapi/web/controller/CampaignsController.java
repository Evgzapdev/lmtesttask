package com.evgzap.loopmerestapi.web.controller;

import com.evgzap.loopmerestapi.model.ErrorResponse;
import com.evgzap.loopmerestapi.model.dto.CampaignDto;
import com.evgzap.loopmerestapi.model.input.CampaignInputDto;
import com.evgzap.loopmerestapi.service.CampaignService;
import com.evgzap.loopmerestapi.value.exception.RecordNotFoundException;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;

@RestController
@AllArgsConstructor
public class CampaignsController {

    private CampaignService service;

    @GetMapping(value = "/campaign/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public CampaignDto getCampaignById(@PathVariable int id) throws RecordNotFoundException {
        return service.getCampaignById(id);
    }

    @PostMapping(value = "/campaign/", produces = MediaType.APPLICATION_JSON_VALUE)
    public CampaignDto createNewCampaign(@RequestBody CampaignInputDto campaign) throws RecordNotFoundException {
        return service.create(campaign);
    }

    @PutMapping(value = "/campaign/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public CampaignDto updateCampaign(@PathVariable int id, @RequestBody CampaignInputDto campaign) throws RecordNotFoundException {
        return service.update(id, campaign);
    }

    @DeleteMapping("/campaign/{id}")
    public void deleteCampaign(@PathVariable int id) throws RecordNotFoundException {
        service.delete(id);
    }

    @ExceptionHandler({SQLException.class})
    public ResponseEntity<ErrorResponse> databaseError() {
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ErrorResponse("Database Error"));
    }

}
