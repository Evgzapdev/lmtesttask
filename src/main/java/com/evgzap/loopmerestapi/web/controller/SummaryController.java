package com.evgzap.loopmerestapi.web.controller;

import com.evgzap.loopmerestapi.model.ErrorResponse;
import com.evgzap.loopmerestapi.model.dto.CampaignShortDto;
import com.evgzap.loopmerestapi.model.pageable.Filter;
import com.evgzap.loopmerestapi.model.pageable.Pageable;
import com.evgzap.loopmerestapi.service.SummariesService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.sql.SQLException;
import java.util.List;

@RestController
@AllArgsConstructor
public class SummaryController {

    private SummariesService summariesService;

    @GetMapping(value = "/summaries/", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<CampaignShortDto> getSummaries(Pageable pageable, Filter filter) {
        return summariesService.getAllCampaignShort(pageable, filter);
    }

    @ExceptionHandler({SQLException.class})
    public ResponseEntity<ErrorResponse> databaseError() {
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ErrorResponse("Database Error"));
    }

}
