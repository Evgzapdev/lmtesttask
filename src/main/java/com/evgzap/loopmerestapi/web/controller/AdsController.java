package com.evgzap.loopmerestapi.web.controller;

import com.evgzap.loopmerestapi.model.ErrorResponse;
import com.evgzap.loopmerestapi.model.dto.AdDto;
import com.evgzap.loopmerestapi.model.input.AdInputDto;
import com.evgzap.loopmerestapi.service.AdService;
import com.evgzap.loopmerestapi.value.exception.RecordNotFoundException;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;

@RestController
@AllArgsConstructor
public class AdsController {

    private AdService service;

    @GetMapping(value = "/ad/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public AdDto getAdById(@PathVariable int id) throws RecordNotFoundException {
        return service.getById(id);
    }

    @PostMapping(value = "/ad/", produces = MediaType.APPLICATION_JSON_VALUE)
    public AdDto createNewAd(@RequestBody AdInputDto ad) throws RecordNotFoundException {
        return service.create(ad);
    }

    @PutMapping(value = "/ad/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public AdDto updateAd(@PathVariable int id, @RequestBody AdInputDto ad) throws RecordNotFoundException {
        return service.update(id, ad);
    }

    @DeleteMapping(value = "/ad/{id}")
    public void deleteAd(@PathVariable int id) throws RecordNotFoundException {
        service.delete(id);
    }

    @ExceptionHandler({SQLException.class})
    public ResponseEntity<ErrorResponse> databaseError() {
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ErrorResponse("Database Error"));
    }
}
