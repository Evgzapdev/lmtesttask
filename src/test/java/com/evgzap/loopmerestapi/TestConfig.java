package com.evgzap.loopmerestapi;

import com.evgzap.loopmerestapi.repository.CampaignRepository;
import com.evgzap.loopmerestapi.service.AdService;
import com.evgzap.loopmerestapi.service.CampaignService;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TestConfig {

    @MockBean
    CampaignRepository campaignRepository;

    @MockBean
    AdService adService;

    @Bean
    public CampaignService campaignService() {
        return new CampaignService(campaignRepository, adService);
    }
}
