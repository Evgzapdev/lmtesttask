package com.evgzap.loopmerestapi.controller;

import com.evgzap.loopmerestapi.TestConfig;
import com.evgzap.loopmerestapi.model.dto.AdDto;
import com.evgzap.loopmerestapi.model.dto.CampaignDto;
import com.evgzap.loopmerestapi.model.input.AdInputDto;
import com.evgzap.loopmerestapi.model.input.CampaignInputDto;
import com.evgzap.loopmerestapi.service.CampaignService;
import com.evgzap.loopmerestapi.value.AdPlatform;
import com.evgzap.loopmerestapi.value.Status;
import com.evgzap.loopmerestapi.web.controller.CampaignsController;
import org.hamcrest.core.Is;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.LocalDateTime;
import java.util.Collections;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(CampaignsController.class)
@ContextConfiguration(classes = {TestConfig.class, CampaignsController.class})
@WebAppConfiguration
public class CampaignControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private CampaignService service;

    @Test
    public void create_campaign_test() throws Exception {
        CampaignDto out = createOutDto();
        BDDMockito.given(service.create(Mockito.any(CampaignInputDto.class))).willReturn(out);

        mvc.perform(post("/campaign/")
                .content("{\"name\":\"name\",\"status\":\"active\",\"start_date\":\"2019-12-05T18:57:16.904Z\",\"end_date\":\"2019-12-05T18:57:16.904Z\",\"ads\":[{\"campaignId\":0,\"name\":\"adMane\",\"status\":\"active\",\"platforms\":[\"android\"],\"assertUrl\":\"url\"}]}")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", Is.is(1)));

    }

    @Test
    public void get_campaign_test() throws Exception {
        CampaignDto out = createOutDto();
        BDDMockito.given(service.getCampaignById(Mockito.anyInt())).willReturn(out);
        mvc.perform(get("/campaign/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", Is.is(1)));
    }

    private CampaignDto createOutDto() {
        AdDto ad = new AdDto(
                1,
                "adMane",
                Status.active,
                Collections.singletonList(AdPlatform.android),
                "url"
        );
        return new CampaignDto(
                1,
                "name",
                Status.active,
                LocalDateTime.now(),
                LocalDateTime.now(),
                Collections.singletonList(ad)
        );
    }
}
