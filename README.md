Requirements:
* docker (with docker-compose)

Build
```bash
 docker-compose build 
```

Run app
```bash
 docker-compose up 
```

Stop app
```bash
 docker-compose stop 
```

Swagger on http://localhost:8080/swagger-ui.html#/